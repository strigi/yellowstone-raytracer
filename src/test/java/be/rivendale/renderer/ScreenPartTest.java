package be.rivendale.renderer;

import org.junit.Assert;
import org.junit.Test;

import java.awt.image.BufferedImage;

public class ScreenPartTest {
    @Test
    public void constructorSetsXCoordinate() throws Exception {
        ScreenPart screenPart = new ScreenPart(10, 100, new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB));
        Assert.assertEquals(10, screenPart.getX());
    }

    @Test
    public void constructorSetsYCoordinate() throws Exception {
        ScreenPart screenPart = new ScreenPart(10, 100, new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB));
        Assert.assertEquals(100, screenPart.getY());
    }

    @Test
    public void constructorSetsImage() throws Exception {
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        ScreenPart screenPart = new ScreenPart(10, 100, image);
        Assert.assertSame(image, screenPart.getImage());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentExceptionWhenTheXCoordinateIsNegative() throws Exception {
        new ScreenPart(-1, 100, new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentExceptionWhenTheYCoordinateIsNegative() throws Exception {
        new ScreenPart(10, -1, new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB));
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentExceptionWhenTheImageIsNull() throws Exception {
        new ScreenPart(10, 100, null);
    }
}
