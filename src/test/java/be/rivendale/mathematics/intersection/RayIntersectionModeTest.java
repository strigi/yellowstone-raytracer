package be.rivendale.mathematics.intersection;

import org.junit.Assert;
import org.junit.Test;

import static be.rivendale.mathematics.intersection.RayIntersectionMode.afterPassThroughPoint;

public class RayIntersectionModeTest {
    @Test
    public void isValidIntersectionPointForAfterPassThroughPointReturnsFalseWhenParameterIsLessThenZero() throws Exception {
        Assert.assertFalse(afterPassThroughPoint.isValidIntersectionPoint(-1));
    }

    @Test
    public void isValidIntersectionPointForAfterPassThroughPointReturnsFalseWhenParameterIsBetweenZeroAndOne() throws Exception {
        Assert.assertFalse(afterPassThroughPoint.isValidIntersectionPoint(0.5));
    }

    @Test
    public void isValidIntersectionPointForAfterPassThroughPointReturnsTrueWhenParameterIsLargerThenOne() throws Exception {
        Assert.assertTrue(afterPassThroughPoint.isValidIntersectionPoint(1.01));
    }

    @Test
    public void isValidIntersectionPointForBetweenOriginAndPassThrougPointReturnsFalseWhenParameterIsLessThenZero() throws Exception {
        Assert.assertFalse(RayIntersectionMode.betweenOriginAndPassThroughPoint.isValidIntersectionPoint(-1));
    }

    @Test
    public void isValidIntersectionPointForBetweenOriginAndPassThrougPointReturnsFalseWhenParameterIsLargerThenOne() throws Exception {
        Assert.assertFalse(RayIntersectionMode.betweenOriginAndPassThroughPoint.isValidIntersectionPoint(2));
    }

    @Test
    public void isValidIntersectionPointForBetweenOriginAndPassThrougPointReturnsTrueWhenParameterIsBetweenZeroAndOne() throws Exception {
        Assert.assertTrue(RayIntersectionMode.betweenOriginAndPassThroughPoint.isValidIntersectionPoint(0.5));
    }
}
