package be.rivendale.mathematics;

import org.junit.Test;

import static be.rivendale.mathematics.MathematicalAssert.assertVectorEquals;

public class TripleTest {
    @Test(expected = IllegalArgumentException.class)
    public void vectorBetweenPointsThrowsIllegalArgumentExceptionWhenFromIsNull() {
        Triple.vectorBetweenPoints(null, new Triple(1, 2, 3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void vectorBetweenPointsThrowsIllegalArgumentExceptionWhenToIsNull() {
        Triple.vectorBetweenPoints(new Triple(1, 2, 3), null);
    }

    @Test
    public void vectorBetweenPointsReturnsVectorAsToMinusFrom() {
        Point from = new Triple(5, 8, 3);
        Point to = new Triple(9, 7, 2);
        Vector vector = Triple.vectorBetweenPoints(from, to);
        assertVectorEquals(to.subtract(from).asVector(), vector);
    }

	@Test(expected = IllegalArgumentException.class)
	public void constructorBasedOnDoubleArrayMustNotBePassedNull() {
	    new Triple(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorBasedOnDoubleArrayMustNotBePassedAnArrayThatIsNotOfLengthThree() {
	    new Triple(new double[4]);
	}

	@Test
	public void constructorBasedOnDoubleArrayInitializesTripleCorrectly() {
		Triple triple = new Triple(new double[]{1, 2, 3});
		MathematicalAssert.assertPointEquals(new Triple(1, 2, 3), triple);
	}
}
