package be.rivendale.mathematics;

import org.junit.Assert;
import org.junit.Test;

public class LineSegmentTest {
    @Test
    public void constructorSetsPointA() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        Assert.assertEquals(new Triple(1, 2, 3), lineSegment.getA());
    }

    @Test
    public void constructorSetsPointB() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        Assert.assertEquals(new Triple(4, 5, 6), lineSegment.getB());
    }

    @Test
    public void directionReturnsVectorParallelToTheLineSegment() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        Vector direction = lineSegment.direction();
        Assert.assertTrue(direction.isParallelTo(Triple.vectorBetweenPoints(lineSegment.getA(), lineSegment.getB())));
    }

    @Test
    public void directionReturnsVectorBetweenPointAAndPointB() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        MathematicalAssert.assertVectorEquals(new Triple(3, 3, 3), lineSegment.direction());
    }

    @Test
    public void pointOnLineSegmentReturnsPointOnLineSegmentIfParameterIsBetweenZeroAndOne() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        Point point = lineSegment.pointOnLineSegment(0.5);
        Assert.assertEquals(new Triple(2.5, 3.5, 4.5), point);
    }

    @Test(expected = IllegalArgumentException.class)
    public void pointOnLineSegmentThrowsIllegalArgumentExceptionWhenParameterIsLargerThenOne() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        lineSegment.pointOnLineSegment(1.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void pointOnLineSegmentThrowsIllegalArgumentExceptionWhenParameterIsLessThenZero() throws Exception {
        LineSegment lineSegment = new LineSegment(new Triple(1, 2, 3), new Triple(4, 5, 6));
        lineSegment.pointOnLineSegment(-1);
    }
}
