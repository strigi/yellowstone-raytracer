package be.rivendale.renderer;

import javax.swing.*;
import java.awt.*;

public class RenderingWindow {

    private final JPanel canvas = new JPanel();

    public RenderingWindow(int width, int height) {
        JFrame frame = new JFrame();
        frame.setTitle("Raytracer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        canvas.setPreferredSize(new Dimension(width, height));
        canvas.setOpaque(true);
        canvas.setBackground(new Color(0, 0, 64));
        frame.getContentPane().add(canvas);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Draws the results of the specified screen part onto the rendering window.
     * @param screenPart The screen part to draw on the window.
     */
    public void drawScreenPart(ScreenPart screenPart) {
        Graphics graphics = canvas.getGraphics();
        graphics.drawImage(screenPart.getImage(), screenPart.getX(), screenPart.getY(), null);
    }
}
