package be.rivendale.renderer.novo;

import be.rivendale.renderer.RenderTask;

import java.awt.*;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;

public class RenderAction extends RecursiveAction {
    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private final Canvas canvas;
    private final Scene scene;
    private final RenderTask task;

    public RenderAction(Scene scene, Canvas canvas) {
        this(scene, canvas, new RenderTask(0, 0, canvas.getWidth(), canvas.getHeight(), canvas.getWidth(), canvas.getHeight(), scene));
    }

    private RenderAction(Scene scene, Canvas canvas, RenderTask task) {
        this.x = task.getX();
        this.y = task.getY();
        this.width = task.getWidth();
        this.height = task.getHeight();
        this.canvas = canvas;
        this.scene = scene;
        this.task = task;
    }

    @Override
    protected void compute() {
        if(mustSplit()) {
            split();
        } else {
            draw();
        }
    }

    private void split() {
        java.util.List<RenderAction> tasks = task.split().stream().map(t -> new RenderAction(scene, canvas, t)).collect(Collectors.toList());
        invokeAll(tasks);
    }

    private void draw() {
        System.out.println(width + "x" + height);
        canvas.update(this::cursor, true);
        canvas.update(this::render, true);
    }

    private void cursor(Graphics2D graphics) {
        graphics.setColor(Color.RED);
        graphics.fillRect(x, y, width, height);
    }

    private void render(Graphics2D graphics) {
        try {
            graphics.drawImage(task.call().getImage(), x, y, null);
        } catch (Exception exception) {
            throw new RuntimeException("Unable to adapt RenderTask to RenderAction", exception);
        }
    }

    private boolean mustSplit() {
        return width == canvas.getWidth() && height == canvas.getHeight();
    }
}
