package be.rivendale.renderer.novo;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.function.Consumer;

class Canvas extends JPanel {
    private final BufferedImage image;

    public Canvas(int width, int height) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.drawImage(image, 0, 0, null);
    }

    public void update(Consumer<Graphics2D> consumer, boolean repaint) {
        Graphics2D graphics = image.createGraphics();
        consumer.accept(graphics);
        graphics.dispose();
        if(repaint) repaint();
    }
}
