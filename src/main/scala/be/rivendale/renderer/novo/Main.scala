package be.rivendale.renderer.novo

import be.rivendale.geometry.{Camera, RawFileLoader}
import be.rivendale.mathematics.{Triple, Vertex}

object Main {
  def main(arguments: Array[String]) {
    val frame = new RenderFrame(1920 / 3, 1080 / 3)
    frame.setVisible(true)
    val scene = new Scene(new Vertex(0, 0, -5), RawFileLoader.loadRawFile("./src/main/data/triangle.raw.txt", new Triple(0, 0, 2)), new Camera())
    while (true) frame.render(scene)
  }
}
