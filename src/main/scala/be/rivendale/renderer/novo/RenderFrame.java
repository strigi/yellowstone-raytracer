package be.rivendale.renderer.novo;

import javax.swing.*;
import java.util.concurrent.ForkJoinPool;

class RenderFrame extends JFrame {
    private static final ForkJoinPool pool = new ForkJoinPool();

    private final Canvas canvas;

    public RenderFrame(int width, int height) {
        super("RenderFrame");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(canvas = new Canvas(width, height));
        setResizable(false);
        pack();
    }

    public void render(Scene scene) {
        pool.invoke(new RenderAction(scene, canvas));
        canvas.repaint();
    }
}
