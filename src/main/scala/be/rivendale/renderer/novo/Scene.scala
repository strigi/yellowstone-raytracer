package be.rivendale.renderer.novo

import be.rivendale.geometry.{Camera, TriangleModel}
import be.rivendale.mathematics.Vertex

class Scene(val light: Vertex, val model: TriangleModel, val camera: Camera)
