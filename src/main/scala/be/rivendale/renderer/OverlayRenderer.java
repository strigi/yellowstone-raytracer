package be.rivendale.renderer;

import be.rivendale.material.Color;

import java.awt.*;

public class OverlayRenderer {
    private static Color[] colors = new Color[] {Color.RED, Color.GREEN, Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.BLUE};

    private Color overlayColor;

    public OverlayRenderer() {
        overlayColor = colors[(int)(Math.random() * (colors.length - 1))];
    }

    public void drawOverlay(ScreenPart screenPart) {
//        Graphics graphics = screenPart.getImage().getGraphics();
//        graphics.setColor(retrieveOverlayColorInAwtFormat());
//		graphics.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
//		graphics.drawString(Thread.currentThread().getName(), 2, 9);
//        graphics.drawRect(0, 0, screenPart.getImage().getWidth() - 1, screenPart.getImage().getHeight() - 1);
    }

    private java.awt.Color retrieveOverlayColorInAwtFormat() {
        int[] integerRgbComponents = overlayColor.toArray();
        return new java.awt.Color(integerRgbComponents[0], integerRgbComponents[1], integerRgbComponents[2]);
    }
}
