package be.rivendale.renderer;

import be.rivendale.geometry.TriangleModel;
import be.rivendale.mathematics.Vertex;
import be.rivendale.novo.Benchmark;
import be.rivendale.renderer.novo.Scene;

import java.util.List;
import java.util.concurrent.*;

public class Renderer {
    private ThreadPoolExecutor executorService;
    private int width;
    private int height;
    private final Scene scene;
    private RenderingWindow window;

    public Renderer(int width, int height, int numberOfConcurrentRenderingThreads, Scene scene) {
        this.width = width;
        this.height = height;
        this.scene = scene;

        window = new RenderingWindow(width, height);
        executorService = new ThreadPoolExecutor(numberOfConcurrentRenderingThreads, numberOfConcurrentRenderingThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()) {
            @Override
            protected void afterExecute(Runnable runnable, Throwable throwable) {
                try {
                    FutureTask<ScreenPart> task = (FutureTask<ScreenPart>) runnable;
                    ScreenPart screenPart = task.get();
                    window.drawScreenPart(screenPart);
                } catch (Exception exception) {
					throw new RuntimeException("Unable to render screen part", exception);
                }
            }
        };
    }

    public void start() {
        Benchmark.benchmark("Scene", new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                renderFrame();
                return Integer.MIN_VALUE;
            }
        }, 10, Integer.MIN_VALUE);
    }

    private void renderFrame() {
        try {
            executorService.invokeAll(createRenderingTasks());
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }

    private List<RenderTask> createRenderingTasks() {
        return new RenderTask(0, 0, width, height, width, height, scene).split();
    }
}
