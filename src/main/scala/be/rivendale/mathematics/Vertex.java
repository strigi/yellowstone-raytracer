package be.rivendale.mathematics;

import be.rivendale.material.Color;
import org.apache.commons.lang.Validate;

public class Vertex extends Triple implements Point {
    private Color color;

    public Vertex(double x, double y, double z) {
        this(x, y, z, Color.WHITE);
    }

    public Vertex(double x, double y, double z, Color color) {
        super(x, y, z);

        Validate.notNull(color, "A vertex requires a color");
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
